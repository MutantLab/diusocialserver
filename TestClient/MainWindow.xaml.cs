﻿#region snippet_MainWindowClass
using System;
using System.Windows;
using Common;
using Microsoft.AspNetCore.SignalR.Client;
using Newtonsoft.Json;



namespace SignalRChatClient
{
    public partial class MainWindow : Window
    {
        private HubConnection _connection = null;

        public MainWindow()
        {
            InitializeComponent();
        }

        private async void connectButton_Click(object sender, RoutedEventArgs e)
        {
            var url = urlTextBox.Text; 

            _connection = new HubConnectionBuilder().WithUrl(url).Build();
           
            _connection.On<string>("Receive", (json) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    messagesList.Items.Add(json);
                });
            });
            
            _connection.Closed += async (error) =>
            {
                this.Dispatcher.Invoke(() =>
                {
                    messagesList.Items.Clear();
                    messagesList.Items.Add($"Closed Server. error({error})");
                    connectButton.IsEnabled = true;
                    sendButton.IsEnabled = false;
                    disconnectButton.IsEnabled = false;
                });
            };
            
            try
            {
                await _connection.StartAsync();
                messagesList.Items.Add("Connection started");
                connectButton.IsEnabled = false;
                sendButton.IsEnabled = true;
                disconnectButton.IsEnabled = true;
            }
            catch (Exception ex)
            {
                messagesList.Items.Add(ex.Message);
            }
        }

        private void clearButton_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                messagesList.Items.Clear();
            }
            catch(Exception ex)
            {
                messagesList.Items.Add(ex.Message);
            }
        }

        private async void DisconnectButton_Click(object sender, RoutedEventArgs e)
        {
            if (_connection == null)
            {

            }
            else
            {
                try
                {
                    await _connection.StopAsync();
                    messagesList.Items.Add("DisConnection");
                    connectButton.IsEnabled = true;
                    sendButton.IsEnabled = false;
                    disconnectButton.IsEnabled = false;

                }
                catch (Exception ex)
                {
                    messagesList.Items.Add(ex.Message);
                }
            }
        }

        private async void sendButton_Click(object sender, RoutedEventArgs e)
        {
            #region snippet_ErrorHandling
            try
            {
                bool TEST_MATCH = false;
                string json = jsonTextBox.Text;
                switch (hubTextBox.Text)
                {
                    case "Login":
                        CS_Login csLogin = new CS_Login();
                        csLogin = JsonConvert.DeserializeObject<CS_Login>(json);
                        json = JsonConvert.SerializeObject(csLogin);
                        break;
                    case "FakeLogin":
                        CS_Login fakeCsLogin = new CS_Login();
                        fakeCsLogin = JsonConvert.DeserializeObject<CS_Login>(json);
                        json = JsonConvert.SerializeObject(fakeCsLogin);
                        break;
                    case "FriendInfo":
                        CS_FriendInfo csFriendInfo = new CS_FriendInfo();
                        csFriendInfo = JsonConvert.DeserializeObject<CS_FriendInfo>(json);
                        csFriendInfo.packet = SocialPacket.CS_FriendInfo;
                        json = JsonConvert.SerializeObject(csFriendInfo);
                        break;
                    case "Match":
                        CS_Match csMatch = new CS_Match();
                        csMatch = JsonConvert.DeserializeObject<CS_Match>(json);
                        csMatch.packet = SocialPacket.CS_Match;
                        json = JsonConvert.SerializeObject(csMatch);
                        break;
                    case "TestMatch":
                        TEST_MATCH = true;
                        break;
                    case "Join":
                        CS_Join csJoin = new CS_Join();
                        csJoin = JsonConvert.DeserializeObject<CS_Join>(json);
                        json = JsonConvert.SerializeObject(csJoin);
                        break;
                    case "Ready":
                        CS_Ready csReady = new CS_Ready();
                        csReady = JsonConvert.DeserializeObject<CS_Ready>(json);
                        csReady.packet = SocialPacket.CS_Ready;
                        json = JsonConvert.SerializeObject(csReady);
                        break;
                    case "ReadyCancel":
                        CS_ReadyCancel csReadyCancel = new CS_ReadyCancel();
                        csReadyCancel = JsonConvert.DeserializeObject<CS_ReadyCancel>(json);
                        csReadyCancel.packet = SocialPacket.CS_ReadyCancel;
                        json = JsonConvert.SerializeObject(csReadyCancel);
                        break;
                    case "SetAgoraUid":
                        CS_SetAgoraUid csSetAgoraUid = new CS_SetAgoraUid();
                        csSetAgoraUid = JsonConvert.DeserializeObject<CS_SetAgoraUid>(json);
                        csSetAgoraUid.packet = SocialPacket.CS_SetAgoraUid;
                        json = JsonConvert.SerializeObject(csSetAgoraUid);
                        break;
                    case "Party":
                        CS_Party csParty = new CS_Party();
                        csParty = JsonConvert.DeserializeObject<CS_Party>(json);
                        csParty.packet = SocialPacket.CS_Party;
                        json = JsonConvert.SerializeObject(csParty);
                        break;
                    case "Start":
                        CS_Start csStart = new CS_Start();
                        csStart = JsonConvert.DeserializeObject<CS_Start>(json);
                        csStart.packet = SocialPacket.CS_Start;
                        json = JsonConvert.SerializeObject(csStart);
                        break;
                    case "MatchCancel":
                        CS_MatchCancel csSMatchCancel = new CS_MatchCancel();
                        csSMatchCancel = JsonConvert.DeserializeObject<CS_MatchCancel>(json);
                        csSMatchCancel.packet = SocialPacket.CS_MatchCancel;
                        json = JsonConvert.SerializeObject(csSMatchCancel);
                        break;
                    case "Leave":
                        CS_Leave csLeave = new CS_Leave();
                        csLeave = JsonConvert.DeserializeObject<CS_Leave>(json);
                        csLeave.packet = SocialPacket.CS_Leave;
                        json = JsonConvert.SerializeObject(csLeave);
                        break;
                    case "Kick":
                        CS_Kick csKick = new CS_Kick();
                        csKick = JsonConvert.DeserializeObject<CS_Kick>(json);
                        csKick.packet = SocialPacket.CS_Kick;
                        json = JsonConvert.SerializeObject(csKick);
                        break;
                    default:
                        break;
                }

                if (!TEST_MATCH)
                {
                    messagesList.Items.Add($"SendJson({json})");
                    await _connection.InvokeAsync(hubTextBox.Text, json);    
                }
                else
                {
                    await _connection.InvokeAsync(hubTextBox.Text);
                }
                
            }
            catch (Exception ex)
            {                
                messagesList.Items.Add(ex.Message);                
            }
            #endregion
        }

        private void hubTextBox_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {

        }
    }
}
#endregion
