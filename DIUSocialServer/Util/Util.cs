using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Hubs;
using DIUSocialServer.Manager;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace DIUSocialServer.Util
{
    public class SocialException : Exception
    {
        public ServerResult serverResult;

        public SocialException()
        {
            
        }

        public SocialException(ServerResult serverResult)
        {
            this.serverResult = serverResult;
        }
    }

    public class InitException : Exception
    {
        public string errMsg;

        public InitException(string errMsg)
        {
            this.errMsg = errMsg;
        }
    }
    
    // # SoclaiServer 에서만 사용
    
    // # Lobby Server Session. 
    public class Session
    {
        public long uid { get; set; }
        public string sid { get; set; }
        public long authed { get; set; }
    }
    
    // # Result
    public class Result
    {
        public SC_Base scBase { get; set; }
        public Dictionary<string, string> unsubscribeGroup { get; set; } // # key: clientId, value: groupName
        public Dictionary<string, string> subscribeGroup { get; set; } // # key: clientId, value: groupName

        // # 브로드캐스팅할경우 특정 clientId 에게만 Ack 보내고 싶을경우. SC_Base 는 추후 클라와 협의. # ex:) Leave Protocol, Kick Protocol
        public bool isWhisperAck { get; set; } 
        public string whisperId { get; set; } 
    }

    public class IngameResponse
    {
        public int RoomNo;
    }

    public static class Sender
    {
        private static async Task BroadCastingAndGroupProcess(Hub hub, Result result, string groupId)
        {

            if (result.subscribeGroup != null && result.subscribeGroup.Count != 0)
            {
                foreach (var subscribe in result.subscribeGroup)
                {
                    Log.Instance.Error($"Subscribe Group. key({subscribe.Key}). value({subscribe.Value})");
                    await hub.Groups.AddToGroupAsync(subscribe.Key, subscribe.Value);    
                }
            }
            
            await hub.Clients.Group(groupId).SendAsync("Receive", Serialize(result.scBase));
            
            if (result.unsubscribeGroup != null && result.unsubscribeGroup.Count != 0)
            {
                foreach (var unsubscribe in result.unsubscribeGroup)
                {
                    Log.Instance.Error($"UnSubscribe Group. key({unsubscribe.Key}). value({unsubscribe.Value})");
                    await hub.Groups.RemoveFromGroupAsync(unsubscribe.Key, unsubscribe.Value);
                }
            }
        }
        
        public static async Task BroadCasting(Hub hub, SC_Base scBase, string groupId)
        {
            Log.Instance.Info($"BroadCasting ({scBase.serverResult}). GroupId({groupId})");
            await hub.Clients.Group(groupId).SendAsync("Receive", Serialize(scBase));
        }
        
        public static async Task BroadCasting(IHubContext<SocialHub> hub, SC_Base scBase, string groupId)
        {
            Log.Instance.Info($"BroadCasting ({scBase.serverResult}). GroupId({groupId})");
            await hub.Clients.Group(groupId).SendAsync("Receive", Serialize(scBase));
        }

        public static async Task Ack(Hub hub, SC_Base scBase)
        {
            Log.Instance.Info($"Ack ({scBase.serverResult})");
            await hub.Clients.Caller.SendAsync("Receive", Serialize(scBase));
        }
        
        public static async Task Whisper(Hub hub, string clientId, SC_Base scBase)
        {
            Log.Instance.Info($"Whisper. clientId({clientId}) ({scBase.serverResult})");
            await hub.Clients.Client(clientId).SendAsync("Receive", Serialize(scBase));
        }
        private static string Serialize<T>(T sc)
        {
            string json = JsonConvert.SerializeObject(sc);
            Log.Instance.Info($"Serialize Json({json})");
            return json;
        }
    }
}