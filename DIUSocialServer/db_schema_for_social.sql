use diu;
create table if not exists diu.tb_friend
(
    uid        bigint not null,
    friendUid  bigint not null,
    created_at bigint null,
    updated_at bigint null,
    primary key (uid, friendUid)
);

create
    definer = admin@`%` procedure diu.get_friend_info(IN i_uid bigint)
BEGIN
    SELECT uid, manner_point, nickname, player_id FROM tb_user WHERE uid = i_uid;
END;