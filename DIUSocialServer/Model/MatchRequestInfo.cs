using System.Collections.Generic;

namespace DIUSocialServer.Model
{
    public class MatchRequestInfo
    {
        public long RequestUid;
        public List<long> PartyMemberList;
        public List<long> IgnoreUidList;
        public int MannerGrade;
        public int StageLevel;
        public string SocialClientId;

    }

    public class MatchResult
    {
        public long HostUid;
        public List<long> PartyMemberList;
        public int StageLevel;
    }

    public class MatchUnit : MatchRequestInfo
    {
        public long UnitId;
        public long RequestTime;
    }
}