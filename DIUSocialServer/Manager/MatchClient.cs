using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Hubs;
using DIUSocialServer.Manager.Handler;
using DIUSocialServer.Model;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace DIUSocialServer.Manager
{
    public class MatchClient
    {
        private static readonly Lazy<MatchClient> _instance = new Lazy<MatchClient>(() => new MatchClient());

        private HubConnection _connection;

        private IHubContext<SocialHub> _hubContext;

        private string _url;
        private int _partyPrefix = 0;
        public static MatchClient Instance => _instance.Value;

        public async Task Initialize(IConfigurationRoot configRoot, object hubContext)
        {
            try
            {
                Log.Instance.Info("Initialize MatchClient");

                _url = configRoot.GetValue<string>("Match:url");
                _partyPrefix = configRoot.GetValue<int>("PartyPrefix");
                
                _hubContext = (IHubContext<SocialHub>) hubContext;

                Log.Instance.Info($"MatchSvr url({_url})");
                
                _connection = new HubConnectionBuilder()
                    .WithUrl(_url)
                    .Build();
                await _connection.StartAsync();
                _connection.On<string>("MatchResult", MatchResult);
                _connection.On<string>("MatchAdd", MatchAdd);
            }
            catch (Exception e)
            {
                Log.Instance.Error($"Exception({e})");
                throw new Exception();
            }
        }

        public async void MatchAdd(string response)
        {
            try
            {
                var matchUnit = JsonConvert.DeserializeObject<MatchUnit>(response);
                Log.Instance.Info($"MatchAdd. matchUnit.uid({matchUnit.RequestUid}). matchUnit.unitId({matchUnit.UnitId})");
                await RedisManager.Instance.SetMatchUnitId(matchUnit.RequestUid, matchUnit.UnitId);
            }
            catch(Exception e)
            {
                Log.Instance.Error($"Exception({e})");
            }
        }

        public async void MatchResult(string response)
        {
            try
            {
                var matchResult = JsonConvert.DeserializeObject<MatchResult>(response);
                Log.Instance.Info($"MatchSvr response({matchResult})");

                var partyInfo = new PartyInfo();
                var lstUserInfo = new List<UserInfo>();

                foreach (var uid in matchResult.PartyMemberList)
                {
                    var userInfoHash = await RedisManager.Instance.GetSocialUserInfo(uid);
                    var userInfo = UserHandler.Deserialize(userInfoHash);
                    lstUserInfo.Add(userInfo);
                }

                partyInfo.lstPartyUser = lstUserInfo;
                
                await PartyHandler.MakeParty(_hubContext, partyInfo, _partyPrefix);
            }
            catch (Exception e)
            {
                Log.Instance.Error($"Exception({e})");
            }
        }

        public async void Match(MatchRequestInfo matchRequestInfo)
        {
            try
            {
                // # MatchServer 에 Connect 되있는 소켓의 ID
                matchRequestInfo.SocialClientId = _connection.ConnectionId;
                
                var req = JsonConvert.SerializeObject(matchRequestInfo);
                await _connection.InvokeAsync("MakeTicket", req);
            }
            catch (Exception e)
            {
                Log.Instance.Error($"Exception({e})");
            }
        }
        
        public async void MatchCancle(long unitId)
        {
            try
            {
                await _connection.InvokeAsync("RemoveTicket", unitId);
            }
            catch (Exception e)
            {
                Log.Instance.Error($"Exception({e})");
            }
        }
    }
}