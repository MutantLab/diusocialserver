using System;
using System.Collections.Generic;
using System.Data;
using System.Threading.Tasks;
using Common;
using Microsoft.AspNetCore.Mvc.TagHelpers;
using Microsoft.Extensions.Configuration;
using MySqlConnector;

namespace DIUSocialServer.Manager
{
    public class DBManager
    {
        private static readonly Lazy<DBManager> _instance = new Lazy<DBManager>(() => new DBManager());
        private MySqlConnection _dbConn;
        
        public static DBManager Instance => _instance.Value;
        
        public async Task Initialize(IConfigurationRoot configRoot)
        {
            try
            {
                var connString = configRoot.GetValue<string>("DB:connString");
                Log.Instance.Info($"DB ConnString({connString})");
                
                _dbConn = new MySqlConnection(connString);
                await _dbConn.OpenAsync();
                                
                Log.Instance.Info("Initialize DB");
            }
            catch (Exception e)
            {
                Log.Instance.Error($"Exception({e})");
                throw new Exception();
            }
        }

        public async Task<PlayerInfo> GetFriendInfo(long uid)
        {
            try
            {
                var playerInfo = new PlayerInfo();
                await using var cmd = _dbConn.CreateCommand();

                cmd.CommandText = @"call get_friend_info(@i_uid);";

                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@i_uid",
                    DbType = DbType.Int64,
                    Value = uid,
                });

                var reader = await cmd.ExecuteReaderAsync();

                using (reader)
                {
                    while (await reader.ReadAsync())
                    {
                        
                        playerInfo.uid = reader.GetInt64("uid");
                        playerInfo.manner_point = reader.GetInt32("manner_point");
                        playerInfo.nickname = reader.GetString("nickname");
                        playerInfo.playerId = reader.GetString("player_id");
                    }
                }
                
                return playerInfo.uid != 0 ? playerInfo : null;    
            }
            catch (Exception e)
            {
                Log.Instance.Error($"Fail Query. Exception({e})");
                return null;
            }
        }

        public async void InsertFreind(long uid, long friendUid)
        {
            try
            {
                await using var cmd = _dbConn.CreateCommand();

                cmd.CommandText = @"call add_friend(@i_uid, @i_friend_uid);";

                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@i_uid",
                    DbType = DbType.Int64,
                    Value = uid,
                });
                cmd.Parameters.Add(new MySqlParameter
                {
                    ParameterName = "@i_friend_uid",
                    DbType = DbType.Int64,
                    Value = friendUid,
                });
                
                await cmd.ExecuteNonQueryAsync();
            }
            catch (Exception e)
            {
                Log.Instance.Error($"Fail Query. Exception({e})");
            }
        }
    }
}