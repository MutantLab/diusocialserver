using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Hubs;
using DIUSocialServer.Util;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace DIUSocialServer.Manager.Handler
{
    public class PartyHandler
    {
        private const int MAX_SAVE_RECENT_PLAY_USER_CNT = 20;

        public static long MakePartyId(long uid, int partyPrefix)
        {
            var tmp = partyPrefix + $"{uid}";
            return long.Parse(tmp);
        }

        public static long GetPartyScore(PartyInfo party)
        {
            // # TODO 임시구현.
            // # TODO 파티의 MMR 정책 확인해야됨.
            return party.lstPartyUser[0].uid;
        }

        public static PartyInfo Reset(PartyInfo partyInfo)
        {
            partyInfo.stageLevel = 1;
            partyInfo.inGameEndPoint = "";
            partyInfo.roomNo = 0;
            partyInfo.partyMasterUid = 0;
            partyInfo.lstPartyUser.Clear();

            return partyInfo;
        }

        public static HashEntry[] ToHashEntry(PartyInfo partyInfo)
        {
            var _lstPartyUser = JsonConvert.SerializeObject(partyInfo.lstPartyUser);
            var hashEntry = new[]
            {
                new HashEntry("partyId", partyInfo.partyId),
                new HashEntry("partyMasterUid", partyInfo.partyMasterUid),
                new HashEntry("lstPartyUser", _lstPartyUser),
                new HashEntry("stageLevel", partyInfo.stageLevel),
                new HashEntry("inGameEndPoint", partyInfo.inGameEndPoint),
                new HashEntry("roomNo", partyInfo.roomNo)
            };
            return hashEntry;
        }

        public static PartyInfo Deserialze(HashEntry[] hashEntry)
        {
            var partyId = hashEntry.FirstOrDefault(x => x.Name == "partyId");
            var partyMasterUid = hashEntry.FirstOrDefault(x => x.Name == "partyMasterUid");
            var lstPartyUser = hashEntry.FirstOrDefault(x => x.Name == "lstPartyUser");
            var stageLevel = hashEntry.FirstOrDefault(x => x.Name == "stageLevel");
            var inGameEndPoint = hashEntry.FirstOrDefault(x => x.Name == "inGameEndPoint");
            var roomNo = hashEntry.FirstOrDefault(x => x.Name == "roomNo");

            var partyInfo = new PartyInfo
            {
                partyId = long.Parse(partyId.Value),
                partyMasterUid = long.Parse(partyMasterUid.Value),
                lstPartyUser = JsonConvert.DeserializeObject<List<UserInfo>>(lstPartyUser.Value),
                stageLevel = int.Parse(stageLevel.Value),
                inGameEndPoint = inGameEndPoint.Value,
                roomNo = int.Parse(roomNo.Value)
            };

            return partyInfo;
        }


        public static long GetPartyMaster(List<UserInfo> lstPartyUser)
        {
            var minUidForMakePartyId = lstPartyUser[0].uid;

            foreach (var userInfo in lstPartyUser)
                if (minUidForMakePartyId > userInfo.uid)
                    minUidForMakePartyId = userInfo.uid;

            return minUidForMakePartyId;
        }

        public static async Task MakeParty(IHubContext<SocialHub> hub, PartyInfo partyInfo, int partyPrefix)
        {
            var scMatch = new SC_Match();

            var partyMasterUid = GetPartyMaster(partyInfo.lstPartyUser);

            partyInfo.partyId = MakePartyId(partyMasterUid, partyPrefix);
            partyInfo.partyMasterUid = partyMasterUid;

            // # IngameServer 에서 방생성.
            var roomNo = await IngameClient.Instance.SendCreateRoom(partyInfo.lstPartyUser);
            if (roomNo < 0)
            {
                var unsubscribe = new Dictionary<string, string>();
                // # Subscribe Group
                foreach (var partyUser in partyInfo.lstPartyUser)
                {
                    await hub.Groups.AddToGroupAsync(partyUser.clientId, $"{partyInfo.partyId}");
                    unsubscribe.Add($"{partyUser.clientId}", $"{partyInfo.partyId}");
                }

                // # Send BroadCasting
                scMatch.serverResult = ServerResult.FailIngameCreatRoom;
                await Sender.BroadCasting(hub, scMatch, $"{partyInfo.partyId}");

                // # UnSubscribe Group
                foreach (var u in unsubscribe) await hub.Groups.RemoveFromGroupAsync(u.Key, u.Value);
            }
            else
            {
                partyInfo.inGameEndPoint = IngameClient.Instance.GetEndPoint();
                partyInfo.roomNo = roomNo;

                // # 예전 파티 groups
                var unsubscribe = new Dictionary<string, string>();

                // # Set Redis
                foreach (var userInfo in partyInfo.lstPartyUser)
                {
                    // # 기존에 파티있다면 파티 unsubscribe
                    var userInfoHash = await RedisManager.Instance.GetSocialUserInfo(userInfo.uid);
                    var oldUserInfo = UserHandler.Deserialize(userInfoHash);
                    if (oldUserInfo.partyId > 0) unsubscribe.Add($"{oldUserInfo.clientId}", $"{oldUserInfo.partyId}");
                    // # 예전 파티 삭제
                    await RedisManager.Instance.DelSocialPratyInfo(oldUserInfo.partyId);

                    UserHandler.Reset(userInfo);
                    userInfo.partyId = partyInfo.partyId;

                    await RedisManager.Instance.SetSocialUserInfo(userInfo.uid, UserHandler.ToHashEntry(userInfo));
                }

                await RedisManager.Instance.SetSocialPartyInfo(partyInfo.partyId, ToHashEntry(partyInfo));

                // # UnSubscribe Group
                foreach (var u in unsubscribe) await hub.Groups.RemoveFromGroupAsync(u.Key, u.Value);

                // # Subscribe Group
                foreach (var partyUser in partyInfo.lstPartyUser)
                    await hub.Groups.AddToGroupAsync(partyUser.clientId, $"{partyInfo.partyId}");

                // # Send BroadCasting
                scMatch.partyInfo = partyInfo;

                await Sender.BroadCasting(hub, scMatch, $"{partyInfo.partyId}");

                foreach (var u in partyInfo.lstPartyUser)
                {
                    // # 자기자신 제외
                    var newParty = (from u2 in partyInfo.lstPartyUser where u.uid != u2.uid select u2.uid)
                        .Select(dummy => (RedisValue) dummy).ToList();

                    var oldRecentPlayUserList = await RedisManager.Instance.GetRecentPlayUser(u.uid);
                    var merge = newParty.Concat(oldRecentPlayUserList).ToArray();
                    RedisManager.Instance.SetRecentPlayUser(u.uid, merge.Distinct().ToArray());
                }
            }
        }

        public static async Task LeaveParty(Hub hub, UserInfo userInfo, bool isUpdateUserInfo)
        {
            var partyHash = await RedisManager.Instance.GetSocialPartyInfo(userInfo.partyId);
            if (partyHash.Length == 0)
            {
                Log.Instance.Error($"Dont Sync PartyInfo({userInfo.partyId})");
            }
            else
            {
                var IS_NEW_PARTY_MASTER = false;
                var partyInfo = Deserialze(partyHash);
                foreach (var partyUser in partyInfo.lstPartyUser.Where(partyUser => partyUser.uid == userInfo.uid))
                {
                    partyInfo.lstPartyUser.Remove(partyUser);
                    if (partyUser.uid == partyInfo.partyMasterUid)
                        // # 새 파티장
                        IS_NEW_PARTY_MASTER = true;
                    break;
                }

                // # 내 파티 unsubsribe
                await hub.Groups.RemoveFromGroupAsync(userInfo.clientId, $"{partyInfo.partyId}");

                // # 내파티 정보 변경
                if (isUpdateUserInfo)
                {
                    UserHandler.Reset(userInfo);
                    await RedisManager.Instance.SetSocialUserInfo(userInfo.uid, UserHandler.ToHashEntry(userInfo));
                }

                if (partyInfo.lstPartyUser.Count == 0)
                {
                    await RedisManager.Instance.DelSocialPratyInfo(partyInfo.partyId);
                }
                else
                {
                    long newPartyMasterUid = 0;

                    if (IS_NEW_PARTY_MASTER)
                    {
                        newPartyMasterUid = GetPartyMaster(partyInfo.lstPartyUser);
                        partyInfo.partyMasterUid = newPartyMasterUid;
                    }

                    await RedisManager.Instance.SetSocialPartyInfo(partyInfo.partyId,
                        ToHashEntry(partyInfo));

                    // # 파티원에게 BroadCasting
                    var scBase = new SC_RenewParty {userInfo = userInfo, newPartyMasterUid = newPartyMasterUid};
                    await Sender.BroadCasting(hub, scBase, $"{partyInfo.partyId}");
                }
            }
        }
    }
}