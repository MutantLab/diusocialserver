using System.Linq;
using System.Threading.Tasks;
using Common;
using Microsoft.AspNetCore.SignalR;
using StackExchange.Redis;

namespace DIUSocialServer.Manager.Handler
{
    public class UserHandler
    {
        public static async Task SetUserInfo(Hub hub, long uid, UserInfo userInfo)
        {
            await RedisManager.Instance.SetSocialUser(hub.Context.ConnectionId, uid);
            await RedisManager.Instance.SetSocialUserInfo(uid, ToHashEntry(userInfo));
        }

        public static async Task DelUserInfo(Hub hub, long uid)
        {
            var h = await RedisManager.Instance.GetSocialUserInfo(uid);
            var userInfo = Deserialize(h);

            await RedisManager.Instance.DelSocialUserInfo(uid);
            await RedisManager.Instance.DelSocialUser(hub.Context.ConnectionId);


            // # 매칭 취소 처리.
            if (userInfo.userState == State.MATCHING)
            {
                if (userInfo.partyId > 0)
                {
                    var partyInfoHash = await RedisManager.Instance.GetSocialPartyInfo(userInfo.partyId);
                    var partyInfo = PartyHandler.Deserialze(partyInfoHash);
                    if (userInfo.uid == partyInfo.partyMasterUid)
                    {
                        foreach (var partyUser in partyInfo.lstPartyUser)
                        {
                            // # 파티원만 매칭 아닌상태로 변경
                            if(partyUser.uid != uid)
                                await RedisManager.Instance.SetSocialUserInfo(partyUser.uid, "userState", (int) State.NONE);
                        }
                    }
                }

                var unitId = await RedisManager.Instance.GetMatchUnitId(userInfo.uid);
                
                MatchClient.Instance.MatchCancle(long.Parse(unitId));                
            }

            // # 파티 처리
            if (userInfo.partyId > 0) await PartyHandler.LeaveParty(hub, userInfo, false);
        }

        public static UserInfo Reset(UserInfo userInfo)
        {
            userInfo.isReady = false;
            userInfo.partyId = 0;
            userInfo.agoraUid = 0;
            userInfo.userState = State.NONE;
            return userInfo;
        }

        public static HashEntry[] ToHashEntry(UserInfo userInfo)
        {
            var hashEntry = new[]
            {
                new HashEntry("uid", userInfo.uid),
                new HashEntry("clientId", userInfo.clientId),
                new HashEntry("partyId", userInfo.partyId),
                new HashEntry("isReady", userInfo.isReady),
                new HashEntry("agoraUid", userInfo.agoraUid),
                new HashEntry("exp", userInfo.exp),
                new HashEntry("manner_point", userInfo.manner_point),
                new HashEntry("userState", (int) userInfo.userState),
                new HashEntry("nickname", userInfo.nickname)
            };
            return hashEntry;
        }

        public static UserInfo Deserialize(HashEntry[] hashEntry)
        {
            var uid = hashEntry.FirstOrDefault(x => x.Name == "uid");
            var clientId = hashEntry.FirstOrDefault(x => x.Name == "clientId");
            var partyId = hashEntry.FirstOrDefault(x => x.Name == "partyId");
            var isReady = hashEntry.FirstOrDefault(x => x.Name == "isReady");
            var agoraUid = hashEntry.FirstOrDefault(x => x.Name == "agoraUid");
            var exp = hashEntry.FirstOrDefault(x => x.Name == "exp");
            var manner_point = hashEntry.FirstOrDefault(x => x.Name == "manner_point");
            var nickname = hashEntry.FirstOrDefault(x => x.Name == "nickname");
            var userState = hashEntry.FirstOrDefault(x => x.Name == "userState");

            var userInfo = new UserInfo
            {
                uid = long.Parse(uid.Value),
                clientId = clientId.Value,
                partyId = long.Parse(partyId.Value),
                isReady = isReady.Value != 0,
                agoraUid = uint.Parse(agoraUid.Value),
                exp = int.Parse(exp.Value),
                manner_point = int.Parse(manner_point.Value),
                userState = (State) int.Parse(userState.Value),
                nickname = nickname.Value
            };

            return userInfo;
        }
    }
}