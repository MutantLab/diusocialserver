using System.Linq;
using Common;
using StackExchange.Redis;

namespace DIUSocialServer.Manager.Handler
{
    public class PlayerHandler
    {
        public static HashEntry[] ToHashEntry(PlayerInfo playerInfo)
        {
            var hashEntry = new[]
            {
                new HashEntry("uid", playerInfo.uid),
                new HashEntry("manner_point", playerInfo.manner_point),
                new HashEntry("nickname", playerInfo.nickname),
                new HashEntry("playerId", playerInfo.playerId),
                new HashEntry("imageNo", playerInfo.imageNo),
                new HashEntry("friendState", (int) playerInfo.friendState),
                new HashEntry("extraValue", playerInfo.extraValue),
                
            };
            return hashEntry;
        }

        public static UserInfo Deserialize(HashEntry[] hashEntry)
        {
            var uid = hashEntry.FirstOrDefault(x => x.Name == "uid");
            var clientId = hashEntry.FirstOrDefault(x => x.Name == "clientId");
            var partyId = hashEntry.FirstOrDefault(x => x.Name == "partyId");
            var isReady = hashEntry.FirstOrDefault(x => x.Name == "isReady");
            var agoraUid = hashEntry.FirstOrDefault(x => x.Name == "agoraUid");
            var exp = hashEntry.FirstOrDefault(x => x.Name == "exp");
            var manner_point = hashEntry.FirstOrDefault(x => x.Name == "manner_point");
            var nickname = hashEntry.FirstOrDefault(x => x.Name == "nickname");
            var userState = hashEntry.FirstOrDefault(x => x.Name == "userState");

            var userInfo = new UserInfo
            {
                uid = long.Parse(uid.Value),
                clientId = clientId.Value,
                partyId = long.Parse(partyId.Value),
                isReady = isReady.Value != 0,
                agoraUid = uint.Parse(agoraUid.Value),
                exp = int.Parse(exp.Value),
                manner_point = int.Parse(manner_point.Value),
                userState = (State) int.Parse(userState.Value),
                nickname = nickname.Value
            };

            return userInfo;
        }
    }
}