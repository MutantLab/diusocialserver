using System;
using System.Globalization;

namespace DIUSocialServer.Manager
{
    public static class TimeManager
    {
        public static long GetCurrentTimeStamp()
        {
            return DateTimeOffset.Now.ToUnixTimeSeconds();
        }

        public static string GetCurrentDateTime()
        {
            return DateTime.Now.ToString("yyyyMMdd");
        }

        public static long GetCurrentTimeStampMS()
        {
            return DateTimeOffset.Now.ToUnixTimeMilliseconds();
        }
    }
}