using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using StackExchange.Redis;

namespace DIUSocialServer.Manager
{
    public class RedisManager
    {
        public enum RedisPurpose
        {
            LOBBY = 0,
            SOCIAL,
            SOCIAL_PERMANENT
        }

        private static readonly Lazy<RedisManager> _instance = new Lazy<RedisManager>(() => new RedisManager());
        private Dictionary<RedisPurpose, IDatabase> _dbConns;

        public static RedisManager Instance => _instance.Value;

        public void Initialize(IConfigurationRoot configRoot)
        {
            try
            {
                Log.Instance.Info("Initialize Redis");

                var host = configRoot.GetValue<string>("Redis:host");
                var port = configRoot.GetValue<string>("Redis:port");

                var _endPoint = host + ":" + port;

                Log.Instance.Info($"Redis endPoint({_endPoint})");

                var options = ConfigurationOptions.Parse(_endPoint);
                options.ConnectRetry = 5;
                options.AllowAdmin = true;

                var redisMaster =
                    ConnectionMultiplexer.Connect(options);

                _dbConns = new Dictionary<RedisPurpose, IDatabase>();

                var _LobbyConn = redisMaster.GetDatabase((int) RedisPurpose.LOBBY);
                _dbConns.Add(RedisPurpose.LOBBY, _LobbyConn);

                var _SocialConn = redisMaster.GetDatabase((int) RedisPurpose.SOCIAL);
                _dbConns.Add(RedisPurpose.SOCIAL, _SocialConn);

                var _SocialPermanentConn = redisMaster.GetDatabase((int) RedisPurpose.SOCIAL_PERMANENT);
                _dbConns.Add(RedisPurpose.SOCIAL_PERMANENT, _SocialPermanentConn);

                var _conn = redisMaster.GetServer(_endPoint);
                DelAllSocialInfo(_conn);
            }
            catch (Exception e)
            {
                Log.Instance.Error($"Exception({e})");
                throw new Exception();
            }
        }

        public IDatabase GetRedis(RedisPurpose pur)
        {
            _dbConns.TryGetValue(pur, out var _db);
            return _db;
        }

        public void DelAllSocialInfo(IServer conn)
        {
            conn.FlushDatabase((int) RedisPurpose.SOCIAL);
        }

        public async Task<HashEntry> GetSocialUserKey_byUid(long uid)
        {
            // return await _db.StringGetAsync(RedisKey.SOCIAL_SESSION(uid));
            var _db = GetRedis(RedisPurpose.SOCIAL);

            var result = new HashEntry();
            var lstSocialUser = await _db.HashGetAllAsync(RedisKey.SOCIAL_USER);
            foreach (var socialUser in lstSocialUser)
                if (socialUser.Value == uid)
                {
                    // # 접속해있음
                    result = socialUser;
                    break;
                }

            return result;
        }


        public async Task<bool> SetSocialUser(string clientId, long uid)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            return await _db.HashSetAsync(RedisKey.SOCIAL_USER, clientId, uid);
        }

        public async Task<RedisValue> GetSocialUser(string clientId)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            return await _db.HashGetAsync(RedisKey.SOCIAL_USER, clientId);
        }

        public async Task<bool> DelSocialUser(string clientId)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            return await _db.HashDeleteAsync(RedisKey.SOCIAL_USER, clientId);
        }

        public async Task SetSocialUserInfo(long uid, HashEntry[] socialUserInfo)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            await _db.HashSetAsync(RedisKey.SOCIAL_USER_INFO(uid), socialUserInfo);
        }

        public async Task<bool> SetSocialUserInfo(long uid, string key, RedisValue rv)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            return await _db.HashSetAsync(RedisKey.SOCIAL_USER_INFO(uid), key, rv);
        }

        public async Task<HashEntry[]> GetSocialUserInfo(long uid)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            return await _db.HashGetAllAsync(RedisKey.SOCIAL_USER_INFO(uid));
        }

        public async Task<bool> DelSocialUserInfo(long uid)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            return await _db.KeyDeleteAsync(RedisKey.SOCIAL_USER_INFO(uid));
        }

        public async Task SetSocialPartyInfo(long partyId, HashEntry[] socialPartyInfo)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            await _db.HashSetAsync(RedisKey.SOCIAL_PARTY(partyId), socialPartyInfo);
        }

        public async Task<bool> SetSocialPartyInfo(long partyId, string key, RedisValue rv)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            return await _db.HashSetAsync(RedisKey.SOCIAL_USER_INFO(partyId), key, rv);
        }

        public async Task<HashEntry[]> GetSocialPartyInfo(long partyId)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            return await _db.HashGetAllAsync(RedisKey.SOCIAL_PARTY(partyId));
        }

        public async Task<bool> DelSocialPratyInfo(long partyId)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            return await _db.KeyDeleteAsync(RedisKey.SOCIAL_PARTY(partyId));
        }

        public async Task<RedisValue> GetLobbyAuth(long uid)
        {
            var _db = GetRedis(RedisPurpose.LOBBY);
            return await _db.StringGetAsync(RedisKey.LOBBY_AUTH(uid));
        }

        public async Task<RedisValue> GetMatchUnitId(long uid)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            return await _db.StringGetAsync(RedisKey.SOCIAL_MATCH_UNITID(uid));
        }

        public async Task<RedisValue> SetMatchUnitId(long uid, long unitId)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL);
            return await _db.StringSetAsync(RedisKey.SOCIAL_MATCH_UNITID(uid), unitId);
        }

        public async Task<RedisValue[]> GetRecentPlayUser(long uid)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL_PERMANENT);
            return await _db.ListRangeAsync(RedisKey.SOCIAL_RECENT_PLAY_USER(uid), 0, 19); // # 최대 20개
        }

        public async void SetRecentPlayUser(long uid, RedisValue[] lstValue)
        {
            var _db = GetRedis(RedisPurpose.SOCIAL_PERMANENT);
            await _db.ListLeftPushAsync(RedisKey.SOCIAL_RECENT_PLAY_USER(uid), lstValue);
            await _db.ListTrimAsync(RedisKey.SOCIAL_RECENT_PLAY_USER(uid), 0, 19); // # 최대 20개
        }

        private class RedisKey
        {
            public const string USER = "DIU:USER";

            public const string SOCIAL_USER = "DIU:SOCIAL:USER"; // Hash key:clientId value:uid

            public const string SOCIAL_MATCH_ALIVE_THREAD = "DIU:SOCIAL:MATCH_ALIVE"; // key: alive . value : true/false

            public static string SOCIAL_USER_INFO(long uid) // Hash key: {SOCIAL_USER}:(유저 id) value: userInfo
            {
                return "DIU:SOCIAL:USER" + ":" + $"{uid}";
            }

            public static string SOCIAL_PARTY(long partyId) // Hash Key:partyId (방장의 uid) value:partyInfo
            {
                return "DIU:SOCIAL:PARTY" + ":" + $"{partyId}";
            }

            public static string LOBBY_AUTH(long uid)
            {
                return "authToken" + ":" + uid;
            }

            public static string SOCIAL_MATCH_UNITID(long uid)
            {
                // key : uid, value : unitId
                return "DIU:SOCIAL:MATCH:UNITID" + ":" + $"{uid}";
            }

            public static string SOCIAL_RECENT_PLAY_USER(long uid) // Hash. key: uid. value: timeStamp
            {
                return "DIU:SOCIAL:RECENT_PLAY_USER" + ":" + $"{uid}";
            }
        }
    }
}