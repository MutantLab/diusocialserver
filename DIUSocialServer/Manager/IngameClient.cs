using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Util;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace DIUSocialServer.Manager
{
    public class IngameClient
    {
        private static readonly Lazy<IngameClient> _instance = new Lazy<IngameClient>(() => new IngameClient());

        private readonly string _createRoomPath = "/createRoom";

        private string _endPoint;

        private HttpClient _httpClient;
        private string _httpEndPoint;
        public static IngameClient Instance => _instance.Value;

        public void Initialize(IConfigurationRoot configRoot)
        {
            try
            {
                Log.Instance.Info("Initialize Ingame");

                var host = configRoot.GetValue<string>("Ingame:host");
                var port = configRoot.GetValue<string>("Ingame:port");
                var httpHost = configRoot.GetValue<string>("Ingame:httpHost");
                var httpPort = configRoot.GetValue<string>("Ingame:httpPort");

                _endPoint = host + ":" + port;

                // # inGameServer http 방생성요청
                _httpEndPoint = "http://" + httpHost + ":" + httpPort + _createRoomPath;

                Log.Instance.Info($"Ingame UDP endPoint({_endPoint})");
                Log.Instance.Info($"Ingame Http endPoint({_httpEndPoint})");

                _httpClient = new HttpClient();
                _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            }
            catch (Exception e)
            {
                Log.Instance.Error($"Exception({e})");
                throw new Exception();
            }
        }

        public string GetEndPoint()
        {
            return _endPoint;
        }

        public async Task<int> SendCreateRoom(List<UserInfo> lstUserInfo)
        {
            var req = new Req();

            foreach (var userInfo in lstUserInfo) req.uidList.Add(userInfo.uid);

            var json = JsonConvert.SerializeObject(req);
            var content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await _httpClient.PostAsync(_httpEndPoint, content);
            Log.Instance.Info($"[InGameClient]Send CreateRoom json({json})");
            var resultJson = response.Content.ReadAsStringAsync().Result;
            var result = JsonConvert.DeserializeObject<IngameResponse>(resultJson);

            return result.RoomNo;
        }

        private class Req
        {
            public readonly List<long> uidList = new List<long>();
        }
    }
}