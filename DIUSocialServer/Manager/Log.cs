using System;
using System.IO;
using Microsoft.Extensions.Configuration;
using NLog;
using NLog.Extensions.Logging;

namespace DIUSocialServer.Manager
{
    public class Log
    {
        private static readonly Lazy<Log> instance = new Lazy<Log>(() => new Log());
        private Logger logger;

        public static Log Instance => instance.Value;

        private Log()
        {
        }

        public void Init(IConfigurationRoot configRoot)
        {
            NLog.LogManager.Configuration = new NLogLoggingConfiguration(configRoot.GetSection("NLog"));
            logger = NLog.LogManager.GetCurrentClassLogger();
        }

        public void Info(object message)
        {
            logger.Info(message);
        }

        public void Info(object message, Exception exception)
        {
            logger.Info(exception, message.ToString);
        }

        public void InfoFormat(string format, params object[] args)
        {
            logger.Info(format, args);
        }

        public void Error(object message)
        {
            logger.Error(message);
        }

        public void Error(string format, params object[] args)
        {
            logger.Error(format, args);
        }

        public void Exception(Exception exception)
        {
            logger.Error(exception, exception.StackTrace);
        }
    }
}