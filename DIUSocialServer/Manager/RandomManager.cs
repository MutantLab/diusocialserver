using System;
namespace DIUSocialServer.Manager
{
    public enum R_TYPE
    {
        ETC = 0,
        END,
    }
    public class RandomManager
    {
        
        static Random[] s_randomList = new Random[(int)R_TYPE.END];

        public static void SetSeed(R_TYPE type, int seed)
        {
            s_randomList[(int)type] = new Random(seed);
        }

        public static int Range(R_TYPE type, int min, int max)
        {
            if(s_randomList[(int)type] == null)
                s_randomList[(int)type] = new Random();

            return s_randomList[(int)type].Next(min, max);
        }

        public static float Range(R_TYPE type, float min, float max)
        {
            if (s_randomList[(int)type] == null)
                s_randomList[(int)type] = new Random();

            float result = (float)s_randomList[(int)type].Next() / (int.MaxValue - 1);
            result = (max - min) * result + min;

            return result;
        }
    }
}