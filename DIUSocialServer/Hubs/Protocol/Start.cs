using System;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Manager;
using DIUSocialServer.Manager.Handler;
using DIUSocialServer.Util;
using Microsoft.AspNetCore.SignalR;

namespace DIUSocialServer.Hubs
{
    public class Start : IDisposable
    {
        private bool _disposed;

        private PartyInfo partyInfo;

        private SC_Start scBase;
        private UserInfo userInfo;

        public void Dispose()
        {
            if (_disposed)
                return;

            GC.SuppressFinalize(this);

            _disposed = true;
        }

        public async Task Process(Hub hub, CS_Start csStart)
        {
            try
            {
                Log.Instance.Info($"Process({csStart})");

                scBase = new SC_Start {serverResult = ServerResult.Success};

                if (csStart == null || csStart.packet != SocialPacket.CS_Start ||
                    csStart.cd == null) throw new SocialException(ServerResult.Fail);

                var uid = await RedisManager.Instance.GetSocialUser(hub.Context.ConnectionId);
                if (uid.IsNullOrEmpty) throw new SocialException(ServerResult.FailAuthSocial);

                var myInfo = await RedisManager.Instance.GetSocialUserInfo(long.Parse(uid));
                if (myInfo.Length == 0)
                    // # 세션정보는 있는데 유저정보는없음.
                    throw new SocialException(ServerResult.DontExistSocialUserInfo);

                userInfo = UserHandler.Deserialize(myInfo);

                if (userInfo.partyId <= 0) throw new SocialException(ServerResult.NotPartyState);

                var partyHash = await RedisManager.Instance.GetSocialPartyInfo(userInfo.partyId);
                if (partyHash.Length == 0) throw new SocialException(ServerResult.DontExistParty);

                partyInfo = PartyHandler.Deserialze(partyHash);

                // # 풀파티아님
                Log.Instance.Info($"GameStart. PartMember Count({partyInfo.lstPartyUser.Count})");

                // # 파장만 가능
                if (partyInfo.partyMasterUid != userInfo.uid)
                    throw new SocialException(ServerResult.NotPartyMaster);

                // # 파장빼고 모두 레디상태야 가능
                var CANT_PROCESS = false;
                foreach (var partyUser in partyInfo.lstPartyUser)
                    if (partyInfo.partyMasterUid != partyUser.uid && partyUser.isReady == false)
                    {
                        CANT_PROCESS = true;
                        break;
                    }

                if (CANT_PROCESS)
                    throw new SocialException(ServerResult.NotYetPartyReady);


                // # IngameServer 에서 방생성.
                var roomNo = await IngameClient.Instance.SendCreateRoom(partyInfo.lstPartyUser);
                if (roomNo <= 0)
                {
                    // # Send BroadCasting
                    scBase.serverResult = ServerResult.FailIngameCreatRoom;
                    await Sender.BroadCasting(hub, scBase, $"{partyInfo.partyId}");
                }
                else
                {
                    partyInfo.inGameEndPoint = IngameClient.Instance.GetEndPoint();
                    partyInfo.roomNo = roomNo;

                    // # Set Redis
                    foreach (var userInfo in partyInfo.lstPartyUser)
                    {
                        userInfo.partyId = partyInfo.partyId;
                        await RedisManager.Instance.SetSocialUserInfo(userInfo.uid, UserHandler.ToHashEntry(userInfo));
                    }

                    await RedisManager.Instance.SetSocialPartyInfo(partyInfo.partyId,
                        PartyHandler.ToHashEntry(partyInfo));

                    // # Send BroadCasting
                    scBase.partyInfo = partyInfo;
                    await Sender.BroadCasting(hub, scBase, $"{partyInfo.partyId}");
                }
            }
            catch (SocialException ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ex.serverResult;
                // res
                await Sender.Ack(hub, scBase);
            }
            catch (Exception ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ServerResult.Fail;
                // res
                await Sender.Ack(hub, scBase);
            }
        }
    }
}