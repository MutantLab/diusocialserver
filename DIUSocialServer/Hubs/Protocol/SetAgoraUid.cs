using System;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Manager;
using DIUSocialServer.Manager.Handler;
using DIUSocialServer.Util;
using Microsoft.AspNetCore.SignalR;

namespace DIUSocialServer.Hubs
{
    public class SetAgoraUid : IDisposable
    {
        private bool _disposed;

        private uint agoraUid;

        private SC_SetAgoraUid scBase;
        private UserInfo userInfo;

        public void Dispose()
        {
            if (_disposed)
                return;

            GC.SuppressFinalize(this);

            _disposed = true;
        }

        public async Task Process(Hub hub, CS_SetAgoraUid csSetAgoraUid)
        {
            try
            {
                Log.Instance.Info($"Process({csSetAgoraUid})");


                scBase = new SC_SetAgoraUid {serverResult = ServerResult.Success};

                if (csSetAgoraUid == null || csSetAgoraUid.packet != SocialPacket.CS_SetAgoraUid ||
                    csSetAgoraUid.cd == null) throw new SocialException(ServerResult.Fail);

                agoraUid = csSetAgoraUid.cd.agoraUid;

                var uid = await RedisManager.Instance.GetSocialUser(hub.Context.ConnectionId);
                if (uid.IsNullOrEmpty) throw new SocialException(ServerResult.FailAuthSocial);

                var myInfo = await RedisManager.Instance.GetSocialUserInfo(long.Parse(uid));
                if (myInfo.Length == 0)
                    // # 세션정보는 있는데 유저정보는없음.
                    throw new SocialException(ServerResult.DontExistSocialUserInfo);

                userInfo = UserHandler.Deserialize(myInfo);
                userInfo.agoraUid = agoraUid;

                var partyId = userInfo.partyId;
                if (partyId <= 0) throw new SocialException(ServerResult.DontExistParty);

                var hashParty = await RedisManager.Instance.GetSocialPartyInfo(partyId);

                var partyInfo = PartyHandler.Deserialze(hashParty);

                

                foreach (var partyUser in partyInfo.lstPartyUser)
                {
                    if (partyUser.uid == userInfo.uid) partyUser.agoraUid = userInfo.agoraUid;
                }

                // # Set Redis
                await RedisManager.Instance.SetSocialUserInfo(userInfo.uid, UserHandler.ToHashEntry(userInfo));
                await RedisManager.Instance.SetSocialPartyInfo(partyInfo.partyId, PartyHandler.ToHashEntry(partyInfo));

                // broadcasting
                scBase.userInfo = userInfo;
                await Sender.BroadCasting(hub, scBase, $"{partyId}");
            }
            catch (SocialException ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ex.serverResult;
                // res
                await Sender.Ack(hub, scBase);
            }
            catch (Exception ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ServerResult.Fail;
                // res
                await Sender.Ack(hub, scBase);
            }
        }
    }
}