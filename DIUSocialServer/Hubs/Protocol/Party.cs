using System;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Manager;
using DIUSocialServer.Manager.Handler;
using DIUSocialServer.Util;
using Microsoft.AspNetCore.SignalR;

namespace DIUSocialServer.Hubs
{
    public class Party : IDisposable
    {
        private bool _disposed;
        private PartyInfo partyInfo;

        private SC_Party scBase;
        private UserInfo userInfo;

        public void Dispose()
        {
            if (_disposed)
                return;

            GC.SuppressFinalize(this);

            _disposed = true;
        }

        public async Task Process(Hub hub, CS_Party csParty)
        {
            try
            {
                Log.Instance.Info($"Process({csParty})");

                scBase = new SC_Party {serverResult = ServerResult.Success};

                if (csParty == null || csParty.packet != SocialPacket.CS_Party ||
                    csParty.cd == null) throw new SocialException(ServerResult.Fail);

                var uid = await RedisManager.Instance.GetSocialUser(hub.Context.ConnectionId);
                if (uid.IsNullOrEmpty) throw new SocialException(ServerResult.FailAuthSocial);

                var myInfo = await RedisManager.Instance.GetSocialUserInfo(long.Parse(uid));
                if (myInfo.Length == 0)
                    // # 세션정보는 있는데 유저정보는없음.
                    throw new SocialException(ServerResult.DontExistSocialUserInfo);

                userInfo = UserHandler.Deserialize(myInfo);

                if (userInfo.partyId <= 0) throw new SocialException(ServerResult.NotPartyState);

                var partyHash = await RedisManager.Instance.GetSocialPartyInfo(userInfo.partyId);
                if (partyHash.Length == 0) throw new SocialException(ServerResult.DontExistParty);

                partyInfo = PartyHandler.Deserialze(partyHash);

                foreach (var partyUser in partyInfo.lstPartyUser)
                {
                    partyUser.isReady = false;

                    var userHashEntry = await RedisManager.Instance.GetSocialUserInfo(partyUser.uid);
                    var partyUserInfo = UserHandler.Deserialize(userHashEntry);

                    // # 파티원상태 저장
                    await RedisManager.Instance.SetSocialUserInfo(partyUser.uid, UserHandler.ToHashEntry(partyUserInfo));
                }

                // # 브로드캐스팅
                scBase.partyInfo = partyInfo;
                await Sender.BroadCasting(hub, scBase, $"{partyInfo.partyId}");
            }
            catch (SocialException ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ex.serverResult;
                // res
                await Sender.Ack(hub, scBase);
            }
            catch (Exception ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ServerResult.Fail;
                // res
                await Sender.Ack(hub, scBase);
            }
        }
    }
}