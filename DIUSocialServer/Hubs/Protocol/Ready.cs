using System;
using System.Linq;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Manager;
using DIUSocialServer.Manager.Handler;
using DIUSocialServer.Util;
using Microsoft.AspNetCore.SignalR;

namespace DIUSocialServer.Hubs
{
    public class Ready : IDisposable
    {
        private bool _disposed;
        private PartyInfo partyInfo;

        private SC_Ready scBase;
        private UserInfo userInfo;

        public void Dispose()
        {
            if (_disposed)
                return;

            GC.SuppressFinalize(this);

            _disposed = true;
        }

        public async Task Process(Hub hub, CS_Ready csReady)
        {
            try
            {
                Log.Instance.Info($"Process({csReady})");

                scBase = new SC_Ready {serverResult = ServerResult.Success};

                if (csReady == null || csReady.packet != SocialPacket.CS_Ready ||
                    csReady.cd == null) throw new SocialException(ServerResult.Fail);

                var uid = await RedisManager.Instance.GetSocialUser(hub.Context.ConnectionId);
                if (uid.IsNullOrEmpty) throw new SocialException(ServerResult.FailAuthSocial);

                var myInfo = await RedisManager.Instance.GetSocialUserInfo(long.Parse(uid));
                if (myInfo.Length == 0)
                    // # 세션정보는 있는데 유저정보는없음.
                    throw new SocialException(ServerResult.DontExistSocialUserInfo);

                userInfo = UserHandler.Deserialize(myInfo);

                if (userInfo.partyId <= 0) throw new SocialException(ServerResult.NotPartyState);

                var partyHash = await RedisManager.Instance.GetSocialPartyInfo(userInfo.partyId);
                if (partyHash.Length == 0) throw new SocialException(ServerResult.DontExistParty);

                partyInfo = PartyHandler.Deserialze(partyHash);

                if (userInfo.uid == partyInfo.partyMasterUid) throw new SocialException(ServerResult.NotPartyUser);

                // # 내상태 변경
                userInfo.isReady = true;

                foreach (var partyUser in partyInfo.lstPartyUser.Where(partyUser => userInfo.uid == partyUser.uid))
                {
                    partyUser.isReady = userInfo.isReady;
                    break;
                }

                // # 내상태 저장
                await RedisManager.Instance.SetSocialUserInfo(userInfo.uid, UserHandler.ToHashEntry(userInfo));

                // # 파티 저장
                await RedisManager.Instance.SetSocialPartyInfo(partyInfo.partyId, PartyHandler.ToHashEntry(partyInfo));

                // # 브로드캐스팅
                scBase.partyInfo = partyInfo;
                await Sender.BroadCasting(hub, scBase, $"{partyInfo.partyId}");
            }
            catch (SocialException ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ex.serverResult;
                // res
                await Sender.Ack(hub, scBase);
            }
            catch (Exception ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ServerResult.Fail;
                // res
                await Sender.Ack(hub, scBase);
            }
        }
    }
}