using System;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Manager;
using DIUSocialServer.Manager.Handler;
using DIUSocialServer.Util;
using Microsoft.AspNetCore.SignalR;

namespace DIUSocialServer.Hubs
{
    public class Leave : IDisposable
    {
        private bool _disposed;
        
        private SC_Leave scBase;
        private UserInfo userInfo;

        public void Dispose()
        {
            if (_disposed)
                return;

            GC.SuppressFinalize(this);

            _disposed = true;
        }
        
        public async Task Process(Hub hub, CS_Leave csLeave)
        {
            try
            {
                Log.Instance.Info($"Process({csLeave})");

                scBase = new SC_Leave {serverResult = ServerResult.Success};

                if (csLeave == null || csLeave.packet != SocialPacket.CS_Leave)
                    throw new SocialException(ServerResult.Fail);

                var uid = await RedisManager.Instance.GetSocialUser(hub.Context.ConnectionId);
                if (uid.IsNullOrEmpty) throw new SocialException(ServerResult.FailAuthSocial);

                var myInfo = await RedisManager.Instance.GetSocialUserInfo(long.Parse(uid));
                if (myInfo.Length == 0)
                    // # 세션정보는 있는데 유저정보는없음.
                    throw new SocialException(ServerResult.DontExistSocialUserInfo);

                userInfo = UserHandler.Deserialize(myInfo);

                await PartyHandler.LeaveParty(hub, userInfo, true);
                await Sender.Ack(hub, scBase);
            }
            catch (SocialException ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ex.serverResult;
                // res
                await Sender.Ack(hub, scBase);
            }
            catch (Exception ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ServerResult.Fail;
                // res
                await Sender.Ack(hub, scBase);
            }
        }
    }
}