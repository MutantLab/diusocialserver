using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Manager;
using DIUSocialServer.Manager.Handler;
using DIUSocialServer.Model;
using DIUSocialServer.Util;
using Microsoft.AspNetCore.SignalR;

namespace DIUSocialServer.Hubs
{
    public class Match : IDisposable
    {
        private bool _disposed;

        private long partyScore;

        private SC_Match scBase;
        private UserInfo userInfo;

        public void Dispose()
        {
            if (_disposed)
                return;

            GC.SuppressFinalize(this);

            _disposed = true;
        }

        public async Task Process(Hub hub, CS_Match csMatch)
        {
            try
            {
                Log.Instance.Info($"Process({csMatch})");

                scBase = new SC_Match {serverResult = ServerResult.Success};

                if (csMatch == null || csMatch.packet != SocialPacket.CS_Match)
                    throw new SocialException(ServerResult.Fail);

                var uid = await RedisManager.Instance.GetSocialUser(hub.Context.ConnectionId);
                if (uid.IsNullOrEmpty) throw new SocialException(ServerResult.FailAuthSocial);

                var myInfo = await RedisManager.Instance.GetSocialUserInfo(long.Parse(uid));
                if (myInfo.Length == 0)
                    // # 세션정보는 있는데 유저정보는없음.
                    throw new SocialException(ServerResult.DontExistSocialUserInfo);

                userInfo = UserHandler.Deserialize(myInfo);

                if (userInfo.userState != State.NONE) throw new SocialException(ServerResult.AlreadyMatching);

                var partyMemberList = new List<long>();

                if (userInfo.partyId > 0)
                {
                    var partyInfoHash = await RedisManager.Instance.GetSocialPartyInfo(userInfo.partyId);
                    var partyInfo = PartyHandler.Deserialze(partyInfoHash);

                    // # 파장 또는 개인만 매칭신청가능
                    if (partyInfo.partyMasterUid != userInfo.uid)
                        throw new SocialException(ServerResult.NotPartyMaster);

                    foreach (var userInfo in partyInfo.lstPartyUser) partyMemberList.Add(userInfo.uid);
                }
                else
                {
                    partyMemberList = new List<long> {userInfo.uid};
                }

                partyScore = userInfo.uid;

                var matchRequestInfo = new MatchRequestInfo
                {
                    RequestUid = userInfo.uid,
                    PartyMemberList = partyMemberList,
                    IgnoreUidList = new List<long>(),
                    MannerGrade = 0,
                    StageLevel = 1
                };

                // # 매칭상태로 변경
                foreach (var u in partyMemberList)
                    await RedisManager.Instance.SetSocialUserInfo(u, "userState", (int) State.MATCHING);

                // # 매칭정보 전송
                MatchClient.Instance.Match(matchRequestInfo);
            }
            catch (SocialException ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ex.serverResult;
                // res
                await Sender.Ack(hub, scBase);
            }
            catch (Exception ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ServerResult.Fail;
                // res
                await Sender.Ack(hub, scBase);
            }
        }
    }
}