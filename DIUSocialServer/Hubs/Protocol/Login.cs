using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Manager;
using DIUSocialServer.Manager.Handler;
using DIUSocialServer.Util;
using Microsoft.AspNetCore.SignalR;
using Newtonsoft.Json;
using StackExchange.Redis;

namespace DIUSocialServer.Hubs
{
    public class Login : IDisposable
    {
        // # TODO 테이블로 뺴야됨.
        private const int LOBBY_AUTH_CHECK_SEC = 30 * 60;
        private bool _disposed;

        private SC_Login scBase;

        public void Dispose()
        {
            if (_disposed)
                return;

            GC.SuppressFinalize(this);

            _disposed = true;
        }

        public async Task Process(Hub hub, CS_Login csLogin, bool isDev)
        {
            scBase = new SC_Login {serverResult = ServerResult.Success};
            try
            {
                Log.Instance.Info($"Process({csLogin})");

                if (!isDev)
                {
                    if (csLogin == null || csLogin.packet != SocialPacket.CS_Login || csLogin.cd == null ||
                        csLogin.cd.uid <= 0)
                    {
                        throw new SocialException(ServerResult.Fail);
                    }
                }
                else
                {
                    Log.Instance.Info("FakeLogin In");
                    if (csLogin == null || csLogin.cd == null || csLogin.cd.uid <= 0)
                    {
                        throw new SocialException(ServerResult.Fail);
                    }
                }

                if (!await IsAuthLobby(csLogin, csLogin.cd.uid, isDev))
                {
                    // # 로비 인증실패
                    throw new SocialException(ServerResult.FailAuthLobby);
                }
                else
                {
                    var result = await RedisManager.Instance.GetSocialUserKey_byUid(csLogin.cd.uid);
                    if (!result.Name.IsNullOrEmpty)
                        // # 기존 유저 세션있다면 지운다.
                        await RedisManager.Instance.DelSocialUser(result.Name);

                    var nickname = csLogin.cd.nickname ?? $"{csLogin.cd.uid}";
                    var manner_point = csLogin.cd.manner_point;
                    var exp = csLogin.cd.exp;

                    // # 가장 마지막 로그인한 유저의 세션으로 갱신한다.
                    var userInfo = new UserInfo
                    {
                        uid = csLogin.cd.uid,
                        clientId = hub.Context.ConnectionId,
                        partyId = 0, // # 0은 파티에 속해있지 않음
                        isReady = false,
                        nickname = nickname,
                        manner_point = manner_point,
                        exp = exp
                    };
                    await UserHandler.SetUserInfo(hub, csLogin.cd.uid, userInfo);
                }
                
                // TEST CODE
                // DBManager.Instance.InsertFreind(123,1);
                // var str = "1";
                // RedisManager.Instance.SetRecentPlayUser(csLogin.cd.uid, str);

                await Sender.Ack(hub, scBase);
            }
            catch (SocialException ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ex.serverResult;
                // res
                await Sender.Ack(hub, scBase);
            }
            catch (Exception ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ServerResult.Fail;
                // res
                await Sender.Ack(hub, scBase);
            }
        }

        private async Task<bool> IsAuthLobby(CS_Login csLogin, long uid, bool isDev)
        {
            // # for dev
            if (isDev)
                return true;

            var strAuth = await RedisManager.Instance.GetLobbyAuth(csLogin.cd.uid);
            if (strAuth.IsNullOrEmpty)
                return false;
            var auth = JsonConvert.DeserializeObject<Session>(strAuth);

            /*   
             * Auth Valid ex
             * authed         : 12000000 : 36000
             * now            : 12000000
             */

            // # uid , sid 가 다르거나 로비인증된시간 + 인증 유효시간  <  소셜서버 현재시간 일경우 인증 실패로 판단한다.
            return auth == null || auth.uid == uid &&
                auth.authed + LOBBY_AUTH_CHECK_SEC >= TimeManager.GetCurrentTimeStampMS();
        }
    }
}