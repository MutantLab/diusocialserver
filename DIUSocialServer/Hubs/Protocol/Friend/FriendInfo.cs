using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Manager;
using DIUSocialServer.Manager.Handler;
using DIUSocialServer.Model;
using DIUSocialServer.Util;
using Microsoft.AspNetCore.SignalR;

namespace DIUSocialServer.Hubs.Friend
{
    public class FriendInfo : IDisposable
    {
        private bool _disposed;
        
        private SC_FriendInfo scBase;
        private UserInfo userInfo;
        public void Dispose()
        {
            if (_disposed)
                return;

            GC.SuppressFinalize(this);

            _disposed = true;
        }
        
        public async Task Process(Hub hub, CS_FriendInfo csFriendInfo)
        {
            try
            {
                Log.Instance.Info($"Process({csFriendInfo})");

                scBase = new SC_FriendInfo() {serverResult = ServerResult.Success};

                if (csFriendInfo == null || csFriendInfo.packet != SocialPacket.CS_FriendInfo)
                    throw new SocialException(ServerResult.Fail);

                var uid = await RedisManager.Instance.GetSocialUser(hub.Context.ConnectionId);
                if (uid.IsNullOrEmpty) throw new SocialException(ServerResult.FailAuthSocial);

                var myInfo = await RedisManager.Instance.GetSocialUserInfo(long.Parse(uid));
                if (myInfo.Length == 0)
                    // # 세션정보는 있는데 유저정보는없음.
                    throw new SocialException(ServerResult.DontExistSocialUserInfo);

                userInfo = UserHandler.Deserialize(myInfo);

                var lstFriend = new List<PlayerInfo>();
                var lstRecentPlayer = new List<PlayerInfo>();

                // # TODO redis 캐싱은 정책알아보고 사용할지 결정한다. 일단 클라 interface 붙이는게 먼저...
                var lstRecentPlayerInfo = await RedisManager.Instance.GetRecentPlayUser(userInfo.uid);
                foreach (var u in lstRecentPlayerInfo)
                {
                    var playerInfo = await DBManager.Instance.GetFriendInfo(long.Parse(u));
                    if (playerInfo != null)
                    {
                        // # TODO userProfile redis 에서 imageNo get
                        // # TODO socialServer redis의 user/party 정보 토대로 friendState 결정
                        lstRecentPlayer.Add(playerInfo);    
                    }
                }

                scBase.lstFriend = lstFriend;
                scBase.lstRecentPlayer = lstRecentPlayer;
                await Sender.Ack(hub, scBase);
                
            }
            catch (SocialException ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ex.serverResult;
                // res
                await Sender.Ack(hub, scBase);
            }
            catch (Exception ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ServerResult.Fail;
                // res
                await Sender.Ack(hub, scBase);
            }
        }
        
    }
}