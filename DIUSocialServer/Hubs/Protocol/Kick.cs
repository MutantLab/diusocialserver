using System;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Manager;
using DIUSocialServer.Manager.Handler;
using DIUSocialServer.Util;
using Microsoft.AspNetCore.SignalR;

namespace DIUSocialServer.Hubs
{
    public class Kick : IDisposable
    {
        private bool _disposed;
        
        private SC_Kick scBase;
        private UserInfo userInfo;
        private long targetUid;
        private UserInfo targetUserInfo;

        public void Dispose()
        {
            if (_disposed)
                return;

            GC.SuppressFinalize(this);

            _disposed = true;
        }
        
        public async Task Process(Hub hub, CS_Kick csKick)
        {
            try
            {
                Log.Instance.Info($"Process({csKick})");

                scBase = new SC_Kick {serverResult = ServerResult.Success};

                if (csKick == null || csKick.packet != SocialPacket.CS_Kick || csKick.cd == null || csKick.cd.targetUid <= 0)
                    throw new SocialException(ServerResult.Fail);

                targetUid = csKick.cd.targetUid;

                var uid = await RedisManager.Instance.GetSocialUser(hub.Context.ConnectionId);
                if (uid.IsNullOrEmpty) throw new SocialException(ServerResult.FailAuthSocial);

                var myInfo = await RedisManager.Instance.GetSocialUserInfo(long.Parse(uid));
                if (myInfo.Length == 0)
                    // # 세션정보는 있는데 유저정보는없음.
                    throw new SocialException(ServerResult.DontExistSocialUserInfo);

                userInfo = UserHandler.Deserialize(myInfo);

                var targetInfo = await RedisManager.Instance.GetSocialUserInfo(targetUid);
                if (targetInfo.Length == 0)
                    throw new SocialException(ServerResult.TargetOffLine);

                targetUserInfo = UserHandler.Deserialize(targetInfo);

                if (userInfo.uid == targetUserInfo.uid)
                    throw new SocialException(ServerResult.CantKickSelf);
                
                if(targetUserInfo.partyId != userInfo.partyId)
                    throw new SocialException(ServerResult.NotPartyUser);

                await PartyHandler.LeaveParty(hub, targetUserInfo, true);

                // # 킥할때 방장이 바뀔일은없지
                var scBaseForTarget = new SC_RenewParty {userInfo = targetUserInfo, newPartyMasterUid = 0};
                await Sender.Whisper(hub, targetUserInfo.clientId, scBaseForTarget);
            }
            catch (SocialException ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ex.serverResult;
                // res
                await Sender.Ack(hub, scBase);
            }
            catch (Exception ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ServerResult.Fail;
                // res
                await Sender.Ack(hub, scBase);
            }
        }
    }
}