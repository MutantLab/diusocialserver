using System;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Manager;
using DIUSocialServer.Manager.Handler;
using DIUSocialServer.Util;
using Microsoft.AspNetCore.SignalR;

namespace DIUSocialServer.Hubs
{
    public class MatchCancel : IDisposable
    {
        private bool _disposed;
        
        private SC_MatchCancel scBase;
        private UserInfo userInfo;
        
        public void Dispose()
        {
            if (_disposed)
                return;

            GC.SuppressFinalize(this);

            _disposed = true;
        }
        public async Task Process(Hub hub, CS_MatchCancel csMatchCancel)
        {
            try
            {
                Log.Instance.Info($"Process({csMatchCancel})");

                scBase = new SC_MatchCancel {serverResult = ServerResult.Success};

                if (csMatchCancel == null || csMatchCancel.packet != SocialPacket.CS_MatchCancel)
                    throw new SocialException(ServerResult.Fail);

                var uid = await RedisManager.Instance.GetSocialUser(hub.Context.ConnectionId);
                if (uid.IsNullOrEmpty) throw new SocialException(ServerResult.FailAuthSocial);

                var myInfo = await RedisManager.Instance.GetSocialUserInfo(long.Parse(uid));
                if (myInfo.Length == 0)
                    // # 세션정보는 있는데 유저정보는없음.
                    throw new SocialException(ServerResult.DontExistSocialUserInfo);

                userInfo = UserHandler.Deserialize(myInfo);

                if (userInfo.partyId > 0)
                {
                    var partyInfoHash = await RedisManager.Instance.GetSocialPartyInfo(userInfo.partyId);
                    var partyInfo = PartyHandler.Deserialze(partyInfoHash);
                    if (userInfo.uid != partyInfo.partyMasterUid)
                    {
                        // # 파장만가능.
                        throw new SocialException(ServerResult.NotPartyMaster);    
                    }
                    // # 파티원 매칭 아닌상태로 변경
                    foreach (var partyUser in partyInfo.lstPartyUser)
                    {
                        await RedisManager.Instance.SetSocialUserInfo(partyUser.uid, "userState", (int) State.NONE);
                    }
                }
                else
                {
                    await RedisManager.Instance.SetSocialUserInfo(userInfo.uid, "userState", (int) State.NONE);
                }
                
                var unitId = await RedisManager.Instance.GetMatchUnitId(userInfo.uid);
                
                MatchClient.Instance.MatchCancle(long.Parse(unitId));

                await Sender.Ack(hub, scBase);
            }
            catch (SocialException ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ex.serverResult;
                // res
                await Sender.Ack(hub, scBase);
            }
            catch (Exception ex)
            {
                Log.Instance.Error($"Exception({ex})");
                scBase.serverResult = ServerResult.Fail;
                // res
                await Sender.Ack(hub, scBase);
            }
        }
        
    }
}