using System;
using System.Threading.Tasks;
using Common;
using DIUSocialServer.Hubs.Friend;
using DIUSocialServer.Manager;
using DIUSocialServer.Manager.Handler;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;

namespace DIUSocialServer.Hubs
{
    public class SocialHub : Hub
    {
        protected string ClientMethod = "Receive";
        public bool isFlag = false;

        public SocialHub(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public override async Task OnConnectedAsync()
        {
            Log.Instance.Info($"OnConnect. ClinetId: {Context.ConnectionId}");
            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            // # Disconnect 할때 clientId 밖에 못받는다. key를 uid 로만 하게되면 전수조사를 하게되므로 두개의 key 를 관리하는방식으로 선택 
            var uid = await RedisManager.Instance.GetSocialUser(Context.ConnectionId);
            if (uid.IsNullOrEmpty)
                // # Login 안하고 다른 패킷올경우도 있으나 로그인하고 정상프로세스에서 예외상황이 생길수 있으니 찍어둔다.
            {
                Log.Instance.Error($"User Session Didnt Sync OR Didnt Login. clientId({Context.ConnectionId})");
            }
            else
            {
                Log.Instance.Info($"DisConnect. ClinetId: {Context.ConnectionId}. uid({long.Parse(uid)})");
                await UserHandler.DelUserInfo(this, long.Parse(uid));
            }

            await base.OnDisconnectedAsync(exception);
        }

        private static CS_Base Deserialize<T>(string json) where T : CS_Base
        {
            Log.Instance.Info($"Recieve Msg json({json})");
            return JsonConvert.DeserializeObject<T>(json);
        }

        #region Hub Method 정의

        public async Task Test(string arg1)
        {
            await Clients.Caller.SendAsync(ClientMethod, "Im Ack");
            await Clients.All.SendAsync(ClientMethod, "Im BroadCasting");
        }

        public async Task Login(string json)
        {
            var csLogin = (CS_Login) Deserialize<CS_Login>(json);
            using var login = new Login();
            await login.Process(this, csLogin, false);
        }

        public async Task FakeLogin(string json)
        {
            var csLogin = (CS_Login) Deserialize<CS_Login>(json);
            using var login = new Login();
            await login.Process(this, csLogin, true);
        }

        public async Task Match(string json)
        {
            var csMatch = (CS_Match) Deserialize<CS_Match>(json);
            using var match = new Match();

            await match.Process(this, csMatch);
        }

        public async Task SetAgoraUid(string json)
        {
            var csSetAgoraUid = (CS_SetAgoraUid) Deserialize<CS_SetAgoraUid>(json);
            using var setAgoraUid = new SetAgoraUid();

            await setAgoraUid.Process(this, csSetAgoraUid);
        }

        public async Task Ready(string json)
        {
            var csReady = (CS_Ready) Deserialize<CS_Ready>(json);
            using var ready = new Ready();

            await ready.Process(this, csReady);
        }

        public async Task ReadyCancel(string json)
        {
            var csReadyCancel = (CS_ReadyCancel) Deserialize<CS_ReadyCancel>(json);
            using var readyCancel = new ReadyCancel();

            await readyCancel.Process(this, csReadyCancel);
        }

        public async Task Party(string json)
        {
            var csParty = (CS_Party) Deserialize<CS_Party>(json);
            using var party = new Party();

            await party.Process(this, csParty);
        }
        
        public async Task Start(string json)
        {
            var csStart = (CS_Start) Deserialize<CS_Start>(json);
            using var start = new Start();

            await start.Process(this, csStart);
        }
        
        public async Task MatchCancel(string json)
        {
            var csMatchCancel = (CS_MatchCancel) Deserialize<CS_MatchCancel>(json);
            using var matchCancel = new MatchCancel();

            await matchCancel.Process(this, csMatchCancel);
        }
        
        public async Task Leave(string json)
        {
            var csLeave = (CS_Leave) Deserialize<CS_Leave>(json);
            using var leave = new Leave();

            await leave.Process(this, csLeave);
        }
        
        public async Task Kick(string json)
        {
            var csKick = (CS_Kick) Deserialize<CS_Kick>(json);
            using var kick = new Kick();

            await kick.Process(this, csKick);
        }

        public async Task FriendInfo(string json)
        {
            var csFriendInfo = (CS_FriendInfo) Deserialize<CS_FriendInfo>(json);
            using var friendInfo = new FriendInfo();
            
            await friendInfo.Process(this, csFriendInfo);
        }

        #endregion Method 정의
    }
}