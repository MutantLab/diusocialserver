using System;
using System.IO;
using System.Threading.Tasks;
using DIUSocialServer.Hubs;
using DIUSocialServer.Manager;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using NLog.Web;

namespace DIUSocialServer
{
    public class Program
    {
        public static async Task Main(string[] args)
        {
            var config = GetConfigurationRoot();

            // 로그 설정. 가장 먼저 실행되어야 함
            Log.Instance.Init(config);

            var host = CreateHostBuilder(args).Build();
            var hubContext = host.Services.GetService(typeof(IHubContext<SocialHub>));

            RedisManager.Instance.Initialize(config);
            await DBManager.Instance.Initialize(config);
            IngameClient.Instance.Initialize(config);
            await MatchClient.Instance.Initialize(config, hubContext);

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            return Host.CreateDefaultBuilder(args)
                .UseNLog()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.ConfigureKestrel(serverOptions => { })
                        .UseStartup<Startup>();
                });
        }

        private static IConfigurationRoot GetConfigurationRoot()
        {
            var filename = "appsettings";
            var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

            if (!string.IsNullOrEmpty(env))
                filename += "." + env;
            else
                filename += ".local";

            filename += ".json";

            // 파일 존재 여부 확인
            var fInfo = new FileInfo(filename);

            if (!fInfo.Exists)
            {
                Console.WriteLine("******************************************");
                Console.WriteLine($"The file was not found. {filename}");
                Console.WriteLine("******************************************");
                throw new FileNotFoundException();
            }

            return new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile(filename, true, true)
                .Build();
        }
    }
}