#!/bin/bash
targetDir=${1}

git branch --show-current >> version.txt
git rev-parse HEAD >> version.txt
scp version.txt ec2-user@bin_repo:~/${targetDir}/diu_social

dotnet publish --framework netcoreapp3.1 -r linux-x64 -c Release /p:PublishSingleFile=true --no-self-contained \
  -warnAsMessage:NETSDK1138,NU1701,NETSDK1099,NETSDK1097
scp bin/Release/netcoreapp3.1/linux-x64/publish/* ec2-user@bin_repo:~/${targetDir}/diu_social
