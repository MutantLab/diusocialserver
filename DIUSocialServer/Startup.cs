using DIUSocialServer.Hubs;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DIUSocialServer
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<KestrelServerOptions>(
                Configuration.GetSection("Kestrel"));

            // # redis backplane (pub / sub)
            var _endPoint = Configuration.GetValue<string>("Redis:host") + ":" +
                            Configuration.GetValue<string>("Redis:port");
            services.AddSignalR().AddStackExchangeRedis(_endPoint);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment()) app.UseDeveloperExceptionPage();

            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<SocialHub>("/socialHub", SetHttpConnectionDispatcherOption);
            });
        }

        private void SetHttpConnectionDispatcherOption(HttpConnectionDispatcherOptions options)
        {
            // # WebSocket만 허용
            options.Transports = HttpTransportType.WebSockets;
        }
    }
}