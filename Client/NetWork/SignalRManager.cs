using System;
using System.ComponentModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR.Client;

namespace Client.NetWork
{
    public class SignalRManager
    {
        private static Lazy<SignalRManager> _instance = new Lazy<SignalRManager>(()=> new SignalRManager());

        private HubConnection _conn;

        private string _host = "localhost";
        private string _port = "5000";
        private string _hub = "hub";

        private string _endPoint;
        
        public static SignalRManager Instance
        {
            get { return _instance.Value;  }
        }

        public SignalRManager()
        {
            _endPoint = "http://" + _host + ":" + _port + "/" + _hub;
        }

        public async Task Initialize()
        {
            Console.WriteLine("Initialize");
            
            _conn = new HubConnectionBuilder().WithUrl(_endPoint).Build();
            await _conn.StartAsync();
        }
    }
}