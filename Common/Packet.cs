using System.Collections.Generic;

namespace Common
{
    public enum SocialPacket
    {
        None = 0,

        CS_Login = 1,
        SC_Login = 2,
        
        CS_Match = 3,
        SC_Match = 4,

        CS_Ready = 7,
        SC_Ready = 8,

        CS_ReadyCancel = 9,
        SC_ReadyCancel = 10,
        
        CS_SetAgoraUid = 11,
        SC_SetAgoraUid = 12,
        
        CS_Party = 13,
        SC_Party = 14,
        
        CS_Start = 15,
        SC_Start = 16,

        SC_RenewParty = 18,
        
        CS_MatchCancel = 19,
        SC_MatchCancel = 20,
        
        CS_Leave = 21,
        SC_Leave = 22,
        
        CS_Death = 23,
        SC_Death = 24,

        CS_Kick = 25,
        SC_Kick = 26,
        
        CS_FriendInfo = 27,
        SC_FriendInfo = 28,

        // # 임시
        CS_Join,
        SC_Join
    }

    public enum ServerResult
    {
        Success = 0,
        Fail,

        FailAuthLobby, // # 로그인시 로비서버 인증실패
        FailAuthSocial, // # 로그인 제외 소셜서버 인증실패
        DontExistSocialUserInfo, // # 세션은 있는데 유저정보는 없음
        
        FailIngameCreatRoom, // 인게임서버 방생성실패
        CantMatching, // 매칭불가능
        AlreadyMatching, // 이미 매칭중

        
        TargetOffLine, // # 상대방 오프라인
        NotPartyState, // # 파티상태아님
        NotPartyUser, // # 파티원아님
        NotPartyMaster, // # 파티장 아님
        DontExistParty, // # 없는 파티정보
        InvalidPartyState, // # 잘못된 파티상태. partyUser정보 sync 안맞음
        TargetNotMyParty, // # 상대방 내파티아님
        CantKickSelf, // # 자기자신강퇴못함
        NotYetPartyReady, // 파티원 올 레디상태아님
        NotFullParty, // 풀파티아님
        NotInGame, // 인게임상태아님
        
        NotMatchingStateParty, // 파티 매칭상태아닌데 매칭됨
        CantParty, // # 자기자신 파티불가능
        TargetAlreadyParty, // # 상대방 이미 파티
        AlreadyParty, // # 이미 파티
        FullParty, // # 풀방
    }

    // # 클라에서 Req cd 에 묶어서 보낸다고함..
    public class cd
    {
        public long uid;
        public long targetUid;
        public uint agoraUid;
        public string nickname;
        public int manner_point;
        public int exp;
    }

    public class CS_Base
    {
        public SocialPacket packet = SocialPacket.None;
        public cd cd;
    }

    public class SC_Base
    {
        public SocialPacket packet = SocialPacket.None;
        public ServerResult serverResult = ServerResult.Success;
    }

    public class CS_Login : CS_Base
    {
        public CS_Login()
        {
            packet = SocialPacket.CS_Login;
        }
    }

    public class SC_Login : SC_Base
    {
        public SC_Login()
        {
            packet = SocialPacket.SC_Login;
        }
    }

    public class CS_Join : CS_Base
    {
        public long targetUid;

        public CS_Join()
        {
            packet = SocialPacket.CS_Join;
        }
    }

    public class SC_Join : SC_Base
    {
        public PartyInfo partyInfo;

        public SC_Join()
        {
            packet = SocialPacket.SC_Join;
        }
    }

    public class CS_Ready : CS_Base
    {
        public CS_Ready()
        {
            packet = SocialPacket.CS_Ready;
        }
    }

    public class SC_Ready : SC_Base
    {
        public PartyInfo partyInfo;

        public SC_Ready()
        {
            packet = SocialPacket.SC_Ready;
        }
    }

    public class CS_ReadyCancel : CS_Base
    {
        public CS_ReadyCancel()
        {
            packet = SocialPacket.CS_ReadyCancel;
        }
    }

    public class SC_ReadyCancel : SC_Base
    {
        public PartyInfo partyInfo;

        public SC_ReadyCancel()
        {
            packet = SocialPacket.SC_ReadyCancel;
        }
    }
    
    public class CS_Leave : CS_Base
    {
        public CS_Leave()
        {
            packet = SocialPacket.CS_Leave;
        }
    }

    public class SC_Leave : SC_Base
    {
        public SC_Leave()
        {
            packet = SocialPacket.SC_Leave;
        }
    }
    
    public class CS_Kick : CS_Base
    {
        public long targetUid;
        
        public CS_Kick()
        {
            packet = SocialPacket.CS_Kick;
        }
    }

    public class SC_Kick : SC_Base
    {
        public long uid;

        public SC_Kick()
        {
            packet = SocialPacket.SC_Kick;
        }
    }
    
    public class CS_Match : CS_Base
    {
        public CS_Match()
        {
            packet = SocialPacket.CS_Match;
        }
    }

    public class SC_Match : SC_Base
    {
        public PartyInfo partyInfo;

        public SC_Match()
        {
            packet = SocialPacket.SC_Match;
        }
    }
    
    public class CS_SetAgoraUid : CS_Base
    {
        public CS_SetAgoraUid()
        {
            packet = SocialPacket.CS_SetAgoraUid;
        }
    }

    public class SC_SetAgoraUid : SC_Base
    {
        public UserInfo userInfo;

        public SC_SetAgoraUid()
        {
            packet = SocialPacket.SC_SetAgoraUid;
        }
    }
    
    public class CS_Party : CS_Base
    {
        public CS_Party()
        {
            packet = SocialPacket.CS_Party;
        }
    }

    public class SC_Party : SC_Base
    {
        public PartyInfo partyInfo;

        public SC_Party()
        {
            packet = SocialPacket.SC_Party;
        }
    }
    
    public class CS_Start : CS_Base
    {
        public CS_Start()
        {
            packet = SocialPacket.CS_Start;
        }
    }

    public class SC_Start : SC_Base
    {
        public PartyInfo partyInfo;

        public SC_Start()
        {
            packet = SocialPacket.SC_Start;
        }
    }
    
    public class SC_RenewParty : SC_Base
    {
        public UserInfo userInfo;
        public long newPartyMasterUid; // 파티장 그대로면 음수

        public SC_RenewParty()
        {
            packet = SocialPacket.SC_RenewParty;
        }
    }
    
    public class CS_MatchCancel : CS_Base
    {
        public CS_MatchCancel()
        {
            packet = SocialPacket.CS_MatchCancel;
        }
    }

    public class SC_MatchCancel : SC_Base
    {
        public SC_MatchCancel()
        {
            packet = SocialPacket.SC_MatchCancel;
        }
    }
    
    public class CS_FriendInfo : CS_Base
    {
        public CS_FriendInfo()
        {
            packet = SocialPacket.CS_FriendInfo;
        }
    }

    public class SC_FriendInfo : SC_Base
    {
        public List<PlayerInfo> lstFriend;
        public List<PlayerInfo> lstRecentPlayer;
        
        public SC_FriendInfo()
        {
            packet = SocialPacket.SC_FriendInfo;
        }
    }
}