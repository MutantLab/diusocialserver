using System.Collections.Generic;

namespace Common
{
    public class UserInfo
    {
        public long uid { get; set; }
        public string clientId { get; set; } // # SocialServer ConnectionId. XXX 필요없을것같지만 디버깅을 위해 남겨둔다..
        public long partyId { get; set; } // # 파티 고유번호
        public bool isReady { get; set; }
        public int manner_point { get; set; }
        public int exp { get; set; }
        public string nickname { get; set; }
        
        public State userState { get; set; }

        public uint agoraUid { get; set; } // # agoraUid.
    }

    public class PartyInfo
    {
        public string inGameEndPoint { get; set; }
        public int roomNo { get; set; }
        public int stageLevel { get; set; }
        public long partyId { get; set; }
        public long partyMasterUid { get; set; }
        public List<UserInfo> lstPartyUser { get; set; }
    }

    public class PlayerInfo
    {
        public long uid { get; set; }
        public int manner_point { get; set; }
        public string nickname { get; set; }
        public string playerId { get; set; }
        public int imageNo { get; set; }
        public FriendState friendState { get; set; }

        /*
         * Invite    : 파티원수
         * Join      : 파티원수   
         * Reqeusting: 파티원수
         * Offline   : 최종 접속시간 (unixtimestamp)
         * Reject    : N초
         */
        public long extraValue { get; set; }
    }

    public enum FriendState
    {
        None = 0,
        Invite,
        Join,
        Requesting,
        CantParty,
        Offline,
        InParty,
        Reject,
    }

    public enum State
    {
        NONE = 0,
        MATCHING
    }
}